<?php

namespace Site\ConstraintBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\ConstraintBundle\Entity\ConstraintUser;
use Site\ConstraintBundle\Form\ConstraintUserType;
use Symfony\Component\HttpFoundation\Request;
class ConstraintController extends Controller
{
    public function indexAction(Request $request)
    {
      
        $ConstraintUser = new ConstraintUser();
        $form = $this->createForm(new ConstraintUserType(), $ConstraintUser, [
            'action' => $this->generateUrl('site_constraint_new')
        ]);
        if (Request::METHOD_POST === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                //$this->getUser();
                $ConstraintUser->setName($user);
                $em = $this->get('doctrine.orm.entity_manager');
                $em->persist($ConstraintUser);
                $em->flush();
                
              
            }
        }
        
        return $this->render('SiteConstraintBundle:Constraint:index.html.twig',
                             array(
                                   'form' => $form->createView(),
                                   ));
    }
}
