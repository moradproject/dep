<?php

namespace Site\ConstraintBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Site\ConstraintBundle\Entity\ConstraintUser;
;


/**
 * ConstraintListe
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Site\ConstraintBundle\Entity\ConstraintListeRepository")
 */
class ConstraintListe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ConstraintListe
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set constraintuser
     *
     * @param \Site\ConstraintBundle\Entity\ConstraintUser $constraintuser
     *
     * @return ConstraintListe
     */
    public function setConstraintuser(\Site\ConstraintBundle\Entity\ConstraintUser $constraintuser)
    {
        $this->constraintuser = $constraintuser;

        return $this;
    }

    /**
     * Get constraintuser
     *
     * @return \Site\ConstraintBundle\Entity\ConstraintUser
     */
    public function getConstraintuser()
    {
        return $this->constraintuser;
    }
    
    public function __toString()
    {
        return $this->name;
    }
}
