<?php

namespace Site\ConstraintBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Site\ConstraintBundle\Entity\ConstraintListe;

/**
 * ConstraintUser
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Site\ConstraintBundle\Entity\ConstraintUserRepository")
 */
class ConstraintUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Site\ConstraintBundle\Entity\ConstraintListe")
     * @ORM\JoinColumn(nullable=false)
     */
    private $constraintliste;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ConstraintUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set constraintliste
     *
     * @param \Site\ConstraintBundle\Entity\ConstraintList $constraintliste
     *
     * @return ConstraintUser
     */
    public function setConstraintliste(\Site\ConstraintBundle\Entity\ConstraintListe $constraintliste)
    {
        $this->constraintliste = $constraintliste;

        return $this;
    }

    /**
     * Get constraintliste
     *
     * @return \Site\ConstraintBundle\Entity\ConstraintList
     */
    public function getConstraintliste()
    {
        return $this->constraintliste;
    }
}
